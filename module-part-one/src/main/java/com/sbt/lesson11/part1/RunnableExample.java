package com.sbt.lesson11.part1;

public class RunnableExample {
    public static void main(String[] args) {
        new Runnable() {
            @Override
            public void run() {
                System.out.println("test");
            }
        }.run();

        Runnable test = new Runnable() {
            @Override
            public void run() {
                System.out.println("test");
            }
        };
        System.out.println(test.getClass().getCanonicalName());
    }
}
