package com.sbt.lesson11.part1.tools;

public class Tools {
    public static double cpuBurn() {
        double result = 0;
        for (int i = 0; i < 10_000; i++) {
            result = Math.sin(cpuBurn2(result + i));
        }
        return result;
    }

    public static double cpuBurn2(double in) {
        double result = in;
        for (int i = 0; i < 10_000; i++) {
            result = Math.sin(result + i);
        }
        return result;
    }

}
