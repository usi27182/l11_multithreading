package com.sbt.lesson11.part1;

public class ThreadExceptionExample {
    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
            System.err.println("i'm broken sorry 1");
            throw new RuntimeException("oops 1");
        });
        Thread thread2 = new Thread(() -> {
            System.err.println("i'm broken sorry 2");
            throw new RuntimeException("oops 2");
        });

        System.err.println("start main");

        thread.setUncaughtExceptionHandler((t, e) -> {
            System.err.println("i'v got it");
        });

        Thread.setDefaultUncaughtExceptionHandler((t, e) -> {
            System.err.println("no exception");
        });

        try {
            thread.start();
            thread2.start();
        } catch (Exception e) {
            System.out.println("caught");
        }
        System.err.println("end main");
    }
}
