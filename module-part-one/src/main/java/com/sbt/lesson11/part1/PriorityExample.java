package com.sbt.lesson11.part1;

import com.sbt.lesson11.part1.tools.Tools;

import java.util.Date;

public class PriorityExample {
    public static void main(String[] args) {
        Runnable runnable = () -> {
            while(true) {
                System.out.println(Thread.currentThread().getName() + " " + new Date());
                Tools.cpuBurn();
            }
        };
        Thread thread1 = new Thread(runnable, "th1");
        Thread thread2 = new Thread(runnable, "th2");

        thread1.setPriority(8);
        thread2.setPriority(2);

        thread1.start();
        thread2.start();


    }
}
