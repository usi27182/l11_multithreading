package ru.nanobit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.atomic.AtomicLong;

@State(Scope.Thread)
public class VolatileTest {

    double d1 = 1_000_000_000F;

    int i1;
    volatile int i2;

    @Benchmark
    public double loop1() {
        for (i1 = 0; i1 < 1_000_000 ; i1++) {
            d1 = d1 * d1;
        }
        return d1;
    }

    @Benchmark
    public double loop2() {
        for (i2 = 0; i2 < 1_000_000 ; i2++) {
            d1 = d1 * d1;
        }
        return d1;
    }




    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(VolatileTest.class.getSimpleName())
                .warmupIterations(5)
                .measurementIterations(5)
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}
