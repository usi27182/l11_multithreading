package ru.nanobit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.atomic.AtomicLong;

@State(Scope.Thread)
public class SequenceGeneratorTest {

    double d1 = 1_000_000_000F;

    long i1;
    volatile long i2;
    AtomicLong i3 = new AtomicLong(1L);
    Object lock = new Object();
    long i4 = 1L;

    @Benchmark
    public long loop1() {
        return i1++;
    }

    @Benchmark
    public long loop2() {
        return i2++;
    }

    @Benchmark
    public long loop3() {
        return i3.getAndSet(1L);
    }


    @Benchmark
    public long loop4() {
        long l;
        synchronized (lock) {
            l = i4++;
        }
        return l;
    }


    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(SequenceGeneratorTest.class.getSimpleName())
                .warmupIterations(5)
                .measurementIterations(5)
                .forks(1)
                //.threads(5)
                .build();

        new Runner(opt).run();
    }
}
