package ru.microbyte.multithreading.deadlock;

public class DeadLockLauncher {
    public static void main(String[] args) {
        LeftRightDeadlock deadlock = new LeftRightDeadlock();

        System.out.println("starting ...");

        new Thread(deadlock::leftRight).start();
        new Thread(deadlock::rightLeft).start();

//        for (int i = 0; i < 1000; i++) {
//            new Thread(deadlock::leftRight).start();
//            new Thread(deadlock::rightLeft).start();
//        }

        System.out.println("main end");
    }
}
