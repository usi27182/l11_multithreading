package ru.microbyte.multithreading.deadlock;

import ru.microbyte.multithreading.ThreadUtils;

// Warning: deadlock-prone!
public class LeftRightDeadlock {
    private final Object left = new Object();
    private final Object right = new Object();

    public void leftRight() {
        synchronized (left) {
            ThreadUtils.pause(10);
            synchronized (right) {
                System.out.println("---->");
            }
        }
    }

    public void rightLeft() {
        synchronized (right) {
            ThreadUtils.pause(10);
            synchronized (left) {
                System.out.println("<----");
            }
        }
    }

}