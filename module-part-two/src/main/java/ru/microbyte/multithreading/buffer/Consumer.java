package ru.microbyte.multithreading.buffer;

public class Consumer implements Runnable {
    private SingleElementBuffer buffer;

    public Consumer(SingleElementBuffer buffer) {
        this.buffer = buffer;
    }

    @Override
    public void run() {
        while (true) {
            try {
                int result = buffer.get();
                System.out.println(result + " got");
            } catch (InterruptedException e) {
                System.out.println(Thread.currentThread().getName() + " stopped");
                break;
            }
        }
    }
}
