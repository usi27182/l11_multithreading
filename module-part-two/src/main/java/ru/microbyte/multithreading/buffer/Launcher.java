package ru.microbyte.multithreading.buffer;

public class Launcher {
    public static void main(String[] args) {
        SingleElementBuffer buffer = new SingleElementBuffer();
        new Thread(new Producer(1, 1000, buffer), "producer 1").start();
        new Thread(new Producer(100, 500, buffer), "producer 2").start();
        new Thread(new Consumer(buffer), "consumer").start();

    }
}
