package ru.microbyte.multithreading.buffer;

public class SingleElementBuffer {
    private Integer element = null;

    public synchronized void put(int newElement) throws InterruptedException {
        while (this.element != null) {
            log("put, wait");
            this.wait();
        }

        this.element = newElement;
        log("put success, notify");
        this.notifyAll();
    }

    public synchronized int get() throws InterruptedException {
        while (this.element == null) {
            log("get, wait");
            this.wait();
        }
        Integer result = this.element;
        this.element = null;
        log("get, success, notify");
        this.notifyAll();
        return result;
    }

    private void log(String s) {
        String name = Thread.currentThread().getName();
        System.out.println(String.format("     %-15s - %s", name, s));
    }
}
