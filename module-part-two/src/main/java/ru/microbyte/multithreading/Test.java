package ru.microbyte.multithreading;

import javax.jws.Oneway;
import java.util.concurrent.TimeUnit;

public class Test {

    static Object lock = new Object();
    public static void main(String[] args) throws InterruptedException {

        new Thread(new Runnable() {
            @Override
            public void run() {
                ThreadUtils.pause(2000);
                synchronized (lock) {
                    notifyAll();
                }
            }
        }).start();

        synchronized (lock) {
            Test.class.wait();
        }
    }
}
