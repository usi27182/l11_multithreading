package ru.microbyte.multithreading;

public class SimpleTest {

    public static void main(String[] args) throws InterruptedException {

//        synchronized (new Object()) {
//        }


        //Object lock = new Object();


        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.currentThread().sleep(10000);
                } catch (InterruptedException e) {
                    System.out.println("interrupted");
                    Thread.currentThread().interrupt();
                }

            }
        });
        thread.start();
        ThreadUtils.pause(1000);

        thread.interrupt();
        ThreadUtils.pause(1000);

        System.out.println(thread.isInterrupted());
        System.out.println(thread.isInterrupted());
        System.out.println(thread.isInterrupted());

//
//        System.out.println(thread.interrupted());
//        System.out.println(thread.interrupted());
    }
}
