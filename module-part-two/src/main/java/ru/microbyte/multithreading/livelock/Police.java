package ru.microbyte.multithreading.livelock;

import ru.microbyte.multithreading.ThreadUtils;

public class Police {

    private boolean moneySent = false;

    public void giveRansom(Criminal criminal) {
        while (!criminal.isHostageReleased()) {
            System.out.println("Police: waiting criminal to release hostage");
            ThreadUtils.pause(1000);
        }

        System.out.println("Police: sent money");
        this.moneySent = true;
    }


    public boolean isMoneySent() {
        return this.moneySent;
    }
}