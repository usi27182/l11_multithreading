package ru.microbyte.multithreading.livelock;

public class HostageRescueLivelock {

    static final Police police = new Police();
    static final Criminal criminal = new Criminal();

    public static void main(String[] args) {

        new Thread(() -> police.giveRansom(criminal)).start();

        new Thread(() -> criminal.releaseHostage(police)).start();
    }

}
