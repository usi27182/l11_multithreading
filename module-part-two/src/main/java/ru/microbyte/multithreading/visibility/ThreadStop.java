package ru.microbyte.multithreading.visibility;

import java.util.concurrent.TimeUnit;

public class ThreadStop {
    private static boolean flag; // false

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            System.out.println("thread start");
            int i = 0;
            while(!flag) {
                i++;
            }
            System.out.println("thread stop");
        }).start();

        System.out.println("main start");
        TimeUnit.SECONDS.sleep(2);
        flag = true;
        System.out.println("main stop");
    }
}
