package ru.microbyte.multithreading;

class Widget {
    public synchronized void doSomething() { // +1
        System.out.println(toString() + ": parent do something");
    }
}

class LoggingWidget extends Widget {
    public synchronized void doSomething() { //+1
        System.out.println(toString() + ": calling doSomething");
        super.doSomething();
    }

    @Override
    public String toString() {
        return "LoggingWidget";
    }
}

public class ReenterExample {
    public static void main(String[] args) {
        LoggingWidget loggingWidget = new LoggingWidget();
        loggingWidget.doSomething();
    }
}
