package ru.microbyte.multithreading.account1;

import ru.microbyte.multithreading.ThreadUtils;

import static java.lang.Thread.currentThread;

/**
 * 00
 * 00
 * 00
 * 00
 * 00
 * 00  00000000
 * 00
 * 00
 * 00
 * 00
 *
 */
class MyClass {
    int i;
}


// monitor.
public class AccountExample {

    Object lock = new Object();

    // AccountExample
    public void makeWithdrawal(Account acct, int amt) {
        // acct
        synchronized (lock) {
            if (acct.getBalance() >= amt) {
                //ThreadUtils.pause(10);

                acct.withdraw(amt);
                System.out.println(currentThread().getName() + " - " + amt + ". Balance " + acct.getBalance());
            } else {
                System.out.println(currentThread().getName() + " - Not enough in account to withdraw - " + amt);
            }
        }
    }

    public static void main(String[] args) {
        AccountExample accountExample = new AccountExample();
        Account acct = new Account(100);

        Runnable task = () -> {
            for (int x = 0; x < 10; x++) {
                ThreadUtils.pause(1);
                accountExample.makeWithdrawal(acct, 10);
                if (acct.getBalance() < 0) {
                    System.out.println(currentThread().getName() + " - account is overdrawn!!!");
                }
            }
        };

//        task.run();
        new Thread(task).start();
        new Thread(task).start();
    }
}