package ru.microbyte.multithreading.account1;

import java.util.Date;

public class Account {
    private int balance;
    private Date touch;

    public Account(int balance) {
        this.balance = balance;
    }

    public int getBalance() {
        return balance;
    }
    public void withdraw(int amount) {
        balance = balance - amount;
    }
}
