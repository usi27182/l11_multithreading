package ru.microbyte.multithreading.account;

public class LauncherTransferring {
    public static void main(String[] args) {
        Account from = new Account("acc_1", 800);
        Account to = new Account("acc_2", 200);
        TransferMachine transferMachine = new TransferMachine();

        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                while(true) {
                    transferMachine.transfer(20, from, to);
                    try {
                        Thread.currentThread().sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }, String.format("thread_01_%02d", i)).start();
        }

        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                while(true) {
                    transferMachine.transfer(30, to, from);
                    try {
                        Thread.currentThread().sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }, String.format("thread_02_%02d", i)).start();
        }
    }
}
