package ru.microbyte.multithreading.account;

import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.String.format;

public class TransferMachine {

    Object lock = new Object();

    private static AtomicInteger seq = new AtomicInteger(1);

    public void transfer(int amount, Account from, Account to) {
         synchronized (from) {
            synchronized (to) {
                int i = seq.addAndGet(1);
                if (from.getBalance() > amount) {
                    from.setBalance(from.getBalance() - amount);
                    to.setBalance(to.getBalance() + amount);
                } else {
                    System.err.println(format("%5d: %15s----------------- block", i, Thread.currentThread().getName()));
                }
                System.err.println(format("%5d: %15s - transfer %10d, %s %20d, %s %10d, sum %10d",
                        i,
                        Thread.currentThread().getName(),
                        amount,
                        from.getName(), from.getBalance(),
                        to.getName(), to.getBalance(),
                        from.getBalance() + to.getBalance()));
            }
        }


    }

}
