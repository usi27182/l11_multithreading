package ru.microbyte.multithreading;

import java.util.concurrent.TimeUnit;

public class ThreadUtils {
    public static void pause(int duration) {
        try {
            TimeUnit.MILLISECONDS.sleep(duration);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
