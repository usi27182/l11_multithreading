package ru.microbyte.multithreading;

import java.util.concurrent.TimeUnit;

public class WaitSleepThread {
    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            int id = i;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    hello(id);
                }

            }).start();
        }
    }
    private synchronized static void hello(int id) {
        System.out.println(id + ": * wait - sleep - " + getTime());
        try {
            WaitSleepThread.class.wait(1000);
        } catch (InterruptedException ignore) { }

        System.out.println(id + ": - wait * sleep - " + getTime());

        try {
            TimeUnit.MILLISECONDS.sleep(1000);
        } catch (InterruptedException ignore) { }

        System.out.println(id + ": - wait - sleep * " + getTime());
    }

    private static String getTime () {
        return "" + (System.currentTimeMillis() - 1576_097_069_000L);

    }

}
